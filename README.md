##ThinkShout coding exercise guidelines

####Guidelines:

* Use GitHub, Bitbucket or similar to host your work.
* In a text document in the project or in issues in the project outline the specific requirements needed to implement the above request.
* Use a simple Drupal 8 site as a starting point. Your site with the donation form should ideally work with two installation steps: 1) installing Drupal (this can include composer install) 2) enabling one custom module containing your configuration and custom code.
* You are encouraged to include your thoughts about why you took the approach you did, what was easy or hard, etc.


####Extra credit:
* Allow the user to select from a list of donation amounts or enter a custom value.
* Track the status of the transaction from Stripe in Drupal with a record of the donation.
* Use AJAX to validate the form inputs.

##Jeff's thoughts

###Usage instructions:
* The repo contains two top-level folders: **app** and **db**
	* db contains a complete sql dump of my working example
		* I understand it is not common practice to commit these things, but it works well for this exercise.
		* Due to some considerations (namely, not exporting a content type in code - see notes in Form plugin), importing this sql will be the easiest way to avoid errors with this working example. 
		* You will need to configure a **app/sites/default/settings.php** with your local development database (MySQL) connection strings to access the application.
	* importing this db will allow for a working environment with my project
		* user|pass : *admin|admin*
	* donation form appears a block and page
		* block is present in "sidebar first"
		* page available at "/donate"
	* *Stripe API key is my own personal testing key. Feel free to use (but not abuse) it.*

####Implementation strategy:
* Create module skeleton
	* We will need a module definition, complete with routes for administration
	* For flexibility the form should be exposed both as a page and a block
	* We will need somewhere to store configuration (stripe API keys)
* Create form
	* the form will serve as the main interface for this project
	* it will need to be pulled into both a page and a block, but we should use a singular definition and implementation to reduce code duplication and foster a singular source for improvements in the future
	* the form implementation will handle all validation and Stripe integration
* Render form in both block and route
* Implement Stripe functionality
	* Investigate soltuions for brining in Stripe library
		* Does a module already exist? (yes, but can't get it to work)
		* Can we use the Libraries API to pull it in? (yes, but we'll go simple for this exercise)
	* Implement simple use-cases for Stripe donation
		* Get token
		* Charge card
	* Write a node on successful card charge
* Manual Testing
	* Ensure form validations catch simple (required, num length) errors before submission
	* Let Stripe handle the heavy lifting of card validation (they are better at it than I am)
	* Test error states
		* Validation rules
		* Stripe errors (make sure to catch, handle, and report back)
	* Test success state
		* ensure record is created with fields populated

####General exercise thoughts:

*many **@TODO** comments in the code discussing various decision points and considerations*

#####Successes -
	* Drupal 8 has made some large improvement in terms of coding standards and separation of concerns
		* I really enjoy the new expected module structure and the explicit nature of each component
		* Working with real classes and OOP is a treat after years of procedural implementations
		* Dependency injection is amazing, but documentation is still a bit lacking
	* The exposure of utility classes is much easier (in my opinion) to use than previous version of Drupal
	* It seems a lot of work has gone into implementing best-practices in terms of storing user-generated protected data (API keys, config, etc)
	* Composer looks like a great package manager. This is my first time using it and it already seems like a vast improvement over simple module dependency checks.

#####Challenges -
	* A lot of Drupal 8 documentation is still hard to find or lacking in completeness. This is certainly a problem that will fix itself in the near future, but it makes for a long ramp-up period.
	* This specific exercise assums a lot of local-system dependencies in place. This was a challenge to me only because of the time necessary to get my enviroment up to spec was not insignificant. I had to install, discover, and use Composer, as well as updating my TLS and cert packages to make calls to the Stripe services. None of these things are detrimental, but certainly eat into the time allotted to complete the exercise.
	* There were several sticky points in which something would not work as described in the documentation (see Stripe-api module). The results are working code, but not necessarily done in the "Drupal way." If one wasn't familiar with Drupal, this would be a gigantic hurdle to overcome.
	* I'm sure there are better ways to go about exporting the entire functionality of this project. I need to research and practice these methods to make use of them. As it stands, I provided what I could in the time allotted. I hope there is enough to open a dialog about the theory behind the implementation and how I could leverage more Drupal 8 core functionality to make a project like this more portable.

#####General thoughts -
	* This was ultimately a fun exercise. 
	* Lots of (good) new tech available since I've been out of the Drupal work professionally.
	* Drupal is still Drupal - steep learning curve and high opiniated in implementation.
	* I completed the requirements to the best of my ability with the information provided. In a real implementation, there would be many additional questions and decision points. I did my best to document each of these within the code via comments.

#####POC considerations -
	* I am purposely NOT investigating additional modules (other than the Stripe library) that might already provide some of the functionality. I _think_ the goal of the exercise is to show that I can competently a) write a module and b) make API calls to an external service. If I was investigating a solution for a customer, I would have to look at contributed modules to determine if some or all of this functionality is present.
	* Devel has been installed to assist with the development of this module
	* Many decision points here would required discussion or confirmation from either the customer or internal voice. I have tried to note these as I come across them.
	* For this exercise, I am purposfully using the "standard" install to have _some_ theme information available, as I will not be theming for this exercise. For a real implementation, I would likely include some theme files in the module to pretty this up.
	* I did use AJAX to validate the form, but don't prefer this method, as it kind of "breaks" to tab key and HTML compliance.

#####Noted ommissions from POC -
	* It would be possible to pull user information from the current user to pre-fill some of the credit card form. I'm not sure this is in the scope of the project, but I wanted to note that it would be possible.
	* Ideally, a permission would be created for the scope of this module. I may get to that in the time allotted, but wanted to jot down a quick note incase I do not.
	* I'm choosing not to validate credit card number. I imagine Stripe is much better at that than I am. I will let them handle it.
	* There are many places user configurability would be idea - path to form, success messages, error messages, Stripe descriptions, etc - I have attempted to note where I think there are appropriate with code comments, but none are implemented.
	* I could not get dependency injection to work properly for the Stripe-api module (see notes in form)
	* There would need to be some discussion about where to house the Stripe API key. Options include module config, Key module, or other solutions. I have chosen the Key module, as it is a dependency of the Stripe-api module we are (attempted) using. This would likely be open for discussion at implementation time.
	* I would like to know more about exporting config from Drupal 8 (I know large improvements have been made here), however, I could not accomplish and implement that in the time allotted for the this exercise. 