<?php
/**
 * @file
 * Contains \Drupal\stripe_donation_block\DonationFormController.
 */

namespace Drupal\stripe_donation_block;

use Drupal\Core\Controller\ControllerBase;

class DonationFormController extends ControllerBase {
	public function content() {
	    return \Drupal::formBuilder()->getForm('Drupal\stripe_donation_block\Form\DonationForm');
	}
}

