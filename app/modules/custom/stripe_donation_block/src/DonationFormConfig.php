<?php
/**
 * @file
 * Contains \Drupal\stripe_donation_block\DonationFormConfig.
 */

namespace Drupal\stripe_donation_block;

use Drupal\Core\Controller\ControllerBase;

class DonationFormConfig extends ControllerBase {
	public function content() {
		return \Drupal::formBuilder()->getForm('Drupal\stripe_donation_block\Form\ConfigForm');
	}
}