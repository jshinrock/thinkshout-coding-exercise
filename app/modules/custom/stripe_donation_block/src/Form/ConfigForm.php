<?php
/**
 * @file
 * Contains \Drupal\resume\Form\WorkForm.
 */

namespace Drupal\stripe_donation_block\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class ConfigForm extends FormBase {
  
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'stripe_donation_block_config_form';
  }
  
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // @TODO I have built a little config form to demonstrate how it could be done, however
    //       we already have the Key module as a dependency to this module, which duplicates
    //       this functionality in a more complet manner. I'm leaving this here for discussion
    //       purposes. Options here would be to either set a variable in the db or update the
    //       config of the module. Unfortunately, I'm not familiar enough with Drupal 8 yet
    //       to make a judgement call on which would be preferred.
    $form['stripe_api_key'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Stripe API key'),
      '#description' => $this->t('Deprecated. This functionality is handled by module "key"'),
      '#default_value' => '',
    );

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary', 
    );

    return $form;
  }
  
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @TODO see notes above
    drupal_set_message($this->t('Stripe configuration updated'));
  }
}