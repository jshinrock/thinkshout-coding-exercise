<?php
/**
 * @file
 * Contains \Drupal\resume\Form\WorkForm.
 */

namespace Drupal\stripe_donation_block\Form;

use \Drupal\node\Entity\Node;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\stripe_api\StripeApiService;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CssCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Stripe\Stripe;
use Stripe\Token;
use Stripe\Charge;

class DonationForm extends FormBase {

  public $ajax_error_state;

  public $ajax_success_state;

  // @TODO I could not get this dependecy injection to work as intended
  // https://www.drupal.org/project/stripe_api
  // public function __construct(StripeApiService $stripe_api)
  public function __construct() {

    // @TODO I'm aware this is not the preferred method of requiring dependencies
    //       in a project. However, to progress with this exercise in the alotted time,
    //       I needed to continue this way
    require_once(DRUPAL_ROOT . '/sites/all/libraries/stripe-php/init.php');
    
    // @TODO I had a decision point here. I did build in configuration to the module
    //       to store this key, but it remains unused. Key is a dependency of Stripe_api
    //       and it is more robust than the simple config I built. This would likely
    //       warrant a discussion in a real project.
    $keys = \Drupal::service('key.repository')->getKeys();
    
    // @TODO in an real enviroment with more keys, this definitely wouldn't be good practice
    //       I would probably have some sort of key manager to extract this info
    $this->stripe_public_key = $this->getStripeKey($keys, 'stripe_public_key');
    $this->stripe_secret_key = $this->getStripeKey($keys, 'stripe_secret_key_dev');

    // @TODO I know Drupal 8 has a much more robust config mechanism, but I could not
    //       get it to function correctly in the time of this exercise. I will have to 
    //       read more documentation about proper usage
    // $config = \Drupal::config('stripe_donation_block.settings');
    // $this->stripe_secret_key = $config->get('stripe_test_api_key');
    
    // @TODO this is probably an unecessary step if the dependecy injection was working
    Stripe::setApiKey($this->stripe_secret_key);

    // @TODO declaring these here for easy reuse and editing
    $this->ajax_success_state = array(
      'css' => array('border' => '1px solid green'),
      'text' => array('font-size' => '0'),
    );
    $this->ajax_error_state = array(
      'css' => array('border' => '1px solid red'),
      'text' => array('color' => 'red', 'font-size' => '.8em'),
    );
  }

  protected function getStripeKey($keys, $name) {
    foreach ($keys as $key => $value) {
      if ($key == $name) {
        return $value->getKeyValue();
      }
    }
  }
  
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'stripe_donation_block_form';
  }
  
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['donation_amount'] = array(
      '#type' => 'fieldset',
      '#title' => 'Donation Amount',
      '#collapsible' => FALSE,
      '#collapsed' => FALSE
    );

    $form['donation_amount']['donate_form_block_amount'] = array(
      '#type' => 'radios',
      '#options' => $this->getRadioValues(),
      '#required' => TRUE,
      '#default_value' => 5,
    );

    $form['donation_amount']['donation_form_block_custom_amount'] = array(
      '#type' => 'number',
      '#min' => 0,
      '#title' => 'Donation amount',
      '#states' => array(
        'visible' => array(
          ':input[name="donate_form_block_amount"]' => array('value' => 0),
        ),
      ),
      '#ajax' => array(
        'callback' => array($this, 'validateCreditCardAmountAjax'),
        'event' => 'change',
        'progress' => array(
          'type' => 'throbber',
          'message' => 'Verifying...',
        ),
      ),
      '#suffix' => '<span class="custom-amount-validated"></span>',
    );

    $form['personal_info'] = array(
      '#type' => 'fieldset',
      '#title' => 'Personal Information',
      '#description' => 'Please provide information as it appears on card',
      '#collapsible' => FALSE,
      '#collapsed' => FALSE
    );

    $form['personal_info']['donate_form_block_first_name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('First Name'),
      '#description' => $this->t(''),
      '#default_value' => '',
      '#required' => TRUE,
    );

    $form['personal_info']['donate_form_block_last_name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Last Name'),
      '#description' => $this->t(''),
      '#default_value' => '',
      '#required' => TRUE,
    );

    $form['personal_info']['donate_form_block_address'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Home address'),
      '#description' => $this->t(''),
      '#default_value' => '',
      '#required' => TRUE,
    );

    $form['personal_info']['donate_form_block_address_two'] = array(
      '#type' => 'textfield',
      '#title' => $this->t(''),
      '#description' => $this->t(''),
      '#default_value' => '',
    );

    $form['personal_info']['donate_form_block_city'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('City'),
      '#description' => $this->t(''),
      '#default_value' => '',
      '#required' => TRUE,
    );

    $form['personal_info']['donate_form_block_state'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('State'),
      '#description' => $this->t(''),
      '#default_value' => '',
      '#required' => TRUE,
    );

    $form['personal_info']['donate_form_block_zip'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Zip'),
      '#description' => $this->t(''),
      '#default_value' => '',
      '#required' => TRUE,
    );

    $form['cc_info'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Credit Card information'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );

    $form['cc_info']['donate_form_block_cc_num'] = array(
      '#type' => 'number',
      '#title' => $this->t('Card number'),
      '#description' => $this->t(''),
      '#default_value' => '',
      '#required' => TRUE,
      '#size' => 16,
    );

    $form['cc_info']['donate_form_block_cc_exp_month'] = array(
      '#type' => 'number',
      '#title' => $this->t('Expiration month'),
      '#description' => $this->t(''),
      '#default_value' => '',
      '#required' => TRUE,
      '#size' => 2,
      '#ajax' => array(
        'callback' => array($this, 'validateCreditCardMonthAjax'),
        'event' => 'change',
        'progress' => array(
          'type' => 'throbber',
          'message' => 'Verifying...',
        ),
      ),
      '#suffix' => '<span class="cc-month-validated"></span>',
    );

    $form['cc_info']['donate_form_block_cc_exp_year'] = array(
      '#type' => 'number',
      '#title' => $this->t('Expiration year'),
      '#description' => $this->t(''),
      '#default_value' => '',
      '#required' => TRUE,
      '#size' => 4,
      '#min' => date('Y'),
      '#ajax' => array(
        'callback' => array($this, 'validateCreditCardYearAjax'),
        'event' => 'change',
        'progress' => array(
          'type' => 'throbber',
          'message' => 'Verifying...',
        ),
      ),
      '#suffix' => '<span class="cc-year-validated"></span>',
    );

    $form['cc_info']['donate_form_block_cc_cvc'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Verification code'),
      '#description' => $this->t(''),
      '#default_value' => '',
      '#required' => TRUE,
      '#size' => 3,
      '#ajax' => array(
        'callback' => array($this, 'validateCreditCardCVCAjax'),
        'event' => 'change',
        'progress' => array(
          'type' => 'throbber',
          'message' => 'Verifying...',
        ),
      ),
      '#suffix' => '<span class="cc-cvc-validated"></span>',
    );

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Donate!'),
      '#button_type' => 'primary', 
    );

    return $form;
  }

  private function getRadioValues() {
    return array(
      5 => '$5',
      10 => '$10',
      20 => '$20',
      50 => '$50',
      0 => 'Custom',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // @TODO leaving server-side valiation in place incase AJAX fails
    $vals = $form_state->getValues();
    $exp_year = $form_state->getValue('donate_form_block_cc_exp_year');
    $exp_month = $form_state->getValue('donate_form_block_cc_exp_month');
    $cvc = $form_state->getValue('donate_form_block_cc_cvc');
    $amount = $form_state->getValue('donate_form_block_amount');
    $custom_amount = $form_state->getValue('donation_form_block_custom_amount');

    if (strlen((string)$exp_year) != 4) {
      $msg = t('Invalid year');
      $form_state->setErrorByName('donate_form_block_cc_exp_year', $msg);
    }
    
    if (strlen((string)$exp_month) != 2) {
      $msg = t('Expiration month must be in format MM');
      $form_state->setErrorByName('donate_form_block_cc_exp_month', $msg);
    }

    if ($exp_month < 1 || $exp_month > 12) {
      $msg = t('Expiration month must be between 01 and 12');
      $form_state->setErrorByName('donate_form_block_cc_exp_month', $msg);
    }

    if (strlen((string)$cvc) != 3) {
      $msg = t('Invalid CVC');
      $form_state->setErrorByName('donate_form_block_cc_cvc', $msg);
    }

    if ($amount == 0 && $custom_amount == 0) {
      $msg  = t('Custom amount not entered');
      $form_state->setErrorByName('donation_form_block_custom_amount', $msg);
    }

    return;
  }


  // @TODO begin ajax validation functionality
  public function validateCreditCardYearAjax(array &$form, FormStateInterface $form_state) {
    $valid = $this->validateCreditCardYear($form, $form_state);
    if ($valid) {
      $css = $this->ajax_success_state['css'];
      $text = $this->ajax_success_state['text'];
      $message = $this->t('Valid');
    } else {
      $css = $this->ajax_error_state['css'];
      $text = $this->ajax_error_state['text'];
      $message = $this->t('Enter a valid year in format YYYY');
    }
    $response = new AjaxResponse();
    $response->addCommand(new CssCommand('#edit-donate-form-block-cc-exp-year', $css));
    $response->addCommand(new HtmlCommand('.cc-year-validated', $message));
    $response->addCommand(new CssCommand('.cc-year-validated', $text));
    return $response;
  }

  public function validateCreditCardYear(array &$form, FormStateInterface $form_state) {
    $vals = $form_state->getValues();
    $exp_year = $form_state->getValue('donate_form_block_cc_exp_year');
    if (strlen((string)$exp_year) != 4) {
      return FALSE;
    }
    return TRUE;
  }

  public function validateCreditCardMonthAjax(array &$form, FormStateInterface $form_state) {
    $valid = $this->validateCreditCardMonth($form, $form_state);
    if ($valid) {
      $css = $this->ajax_success_state['css'];
      $text = $this->ajax_success_state['text'];
      $message = $this->t('Valid');
    } else {
      $css = $this->ajax_error_state['css'];
      $text = $this->ajax_error_state['text'];
      $message = $this->t('Enter a valid month in format MM');
    }
    $response = new AjaxResponse();
    $response->addCommand(new CssCommand('#edit-donate-form-block-cc-exp-month', $css));
    $response->addCommand(new HtmlCommand('.cc-month-validated', $message));
    $response->addCommand(new CssCommand('.cc-month-validated', $text));
    return $response;
  }

  public function validateCreditCardMonth(array &$form, FormStateInterface $form_state) {
    $vals = $form_state->getValues();
    $vals = $form_state->getValues();
    $exp_month = $form_state->getValue('donate_form_block_cc_exp_month');
    if (strlen((string)$exp_month) != 2) {
      return FALSE;
    }
    if ($exp_month < 1 || $exp_month > 12) {
      return FALSE;
    }
    return TRUE;
  }

  public function validateCreditCardCVCAjax(array &$form, FormStateInterface $form_state) {
    $valid = $this->validateCreditCardCVC($form, $form_state);
    if ($valid) {
      $css = $this->ajax_success_state['css'];
      $text = $this->ajax_success_state['text'];
      $message = $this->t('Valid');
    } else {
      $css = $this->ajax_error_state['css'];
      $text = $this->ajax_error_state['text'];
      $message = $this->t('Enter a valid cvc');
    }
    $response = new AjaxResponse();
    $response->addCommand(new CssCommand('#edit-donate-form-block-cc-cvc', $css));
    $response->addCommand(new HtmlCommand('.cc-cvc-validated', $message));
    $response->addCommand(new CssCommand('.cc-cvc-validated', $text));
    return $response;
  }

  public function validateCreditCardCVC(array &$form, FormStateInterface $form_state) {
    $vals = $form_state->getValues();
    $cvc = $form_state->getValue('donate_form_block_cc_cvc');
    if (strlen((string)$cvc) != 3) {
      return FALSE;
    }
    return TRUE;
  }

  public function validateCreditCardAmountAjax(array &$form, FormStateInterface $form_state) {
    $valid = $this->validateCustomAmount($form, $form_state);
    if ($valid) {
      $css = $this->ajax_success_state['css'];
      $text = $this->ajax_success_state['text'];
      $message = $this->t('Valid');
    } else {
      $css = $this->ajax_error_state['css'];
      $text = $this->ajax_error_state['text'];
      $message = $this->t('Enter a custom amount');
    }
    $response = new AjaxResponse();
    $response->addCommand(new CssCommand('#edit-donation-form-block-custom-amount', $css));
    $response->addCommand(new HtmlCommand('.custom-amount-validated', $message));
    $response->addCommand(new CssCommand('.custom-amount-validated', $text));
    return $response;
  }

  public function validateCustomAmount(array &$form, FormStateInterface $form_state) {
    $vals = $form_state->getValues();
    $amount = $form_state->getValue('donate_form_block_amount');
    $custom_amount = $form_state->getValue('donation_form_block_custom_amount');
    if ($amount == 0 && $custom_amount == 0) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @TODO lots of error handling would need to be done in production.
    //       I'm just going to display the error for this exercise
    $vals = $form_state->getValues();
    $amount = $vals['donate_form_block_amount'];
    if ($amount == 0) {
      $amount = $vals['donation_form_block_custom_amount'];
    }
    try {
      $token = $this->getStripeToken($vals);
      $res = $this->chargeCard($token, $amount);
      $this->handleDonateSuccess($res, $vals, $amount);
    } catch (\Stripe\Error\InvalidRequest $e) {
      // Invalid parameters were supplied to Stripe's API
      $this->handleDonateError($e);
    } catch (\Stripe\Error\Authentication $e) {
      // Authentication with Stripe's API failed
      // (maybe you changed API keys recently)
      $this->handleDonateError($e);
    } catch (\Stripe\Error\ApiConnection $e) {
      // Network communication with Stripe failed
      $this->handleDonateError($e);
    } catch (\Stripe\Error\Base $e) {
      // Display a very generic error to the user, and maybe send
      // yourself an email
      $this->handleDonateError($e);
    } catch (Exception $e) {
      $this->handleDonateError($e);
      return FALSE;
    }
  }

  public function handleDonateSuccess($res, $vals, $amount) {
    $record_info = array(
      'name' => $vals['donate_form_block_first_name'] . ' ' . $vals['donate_form_block_last_name'],
    );
    // @TODO store a Drupal node for tracking/record of donation
    $this->storeDonationRecord($res, $record_info);
    $msg = $this->t('Thank you for donating!');
    drupal_set_message($msg);
  }

  public function handleDonateError($err) {
    // @TODO let's log the error to the logger on fail
    \Drupal::logger('stripe_donation_block')->error($err->getMessage());
    $msg = $this->t('There was an error processing your payment. Please try again later.');
    $msg_detail = $err->getMessage();
    $type = 'error';
    drupal_set_message($msg, $type);
    drupal_set_message($msg_detail, $type);
  }

  private function getStripeToken($vals) {
    // @TODO here, I would check localStorage (via some JS) first for a token and return that if exists
    $name = $vals['donate_form_block_first_name'] . ' ' . $vals['donate_form_block_last_name'];
    $card_info = array(
      'card' => array(
        'name' => $name,
        'number' => $vals['donate_form_block_cc_num'],
        'exp_month' => $vals['donate_form_block_cc_exp_month'],
        'exp_year' => $vals['donate_form_block_cc_exp_year'],
        'cvc' => $vals['donate_form_block_cc_cvc'],
      ),
    );
    // @TODO from stripe-php docs
    $res = Token::create($card_info);
    $token = $res['id'];
    return $token;
  }

  private function chargeCard($token, $amount) {
    $info = array(
      'amount' => $this->deriveAmount($amount),
      // @TODO I assume $USD is the only option here, hence the hard-coding
      'currency' => 'usd',
      'card' => $token,
      // @TODO this would be a good place to allow for a custom donation message 
      //       to be set in the module config, however, excluding for this exercise
      'description' => 'Donation',
    );
    // @TODO from the stripe-php docs
    $res = Charge::create($info);
    return $res;
  }

  private function deriveAmount($amount) {
    // @TODO stripe accepts amount in 1c increments
    return intval($amount) * 100;
  }

  private function storeDonationRecord($record, $vals) {
    // @TODO no idea on the requirements here. I'm going to add a 
    //       generic node visible to all for our records. 
    //       If this was for a real customer, I would create a node type (in code) to 
    //       include with the module that would be available on module install. 
    //       As it is, I've created one via the UI to use. I know this isn't tranferrable,
    //       but I hope it serves the purpose for this exercise
    $amount = $record['amount'] / 100;
    $node_info = array(
      'type' => 'stripe_donation',
      'title' => 'Stripe Donation by ' . $vals['name'] . ' [' . $record['id'] . ']',
      'field_name' => $vals['name'],
      'field_status' => $record['status'],
      'field_stripe_record' => $record['id'],
      'field_amount' => $amount,
    );
    $node = Node::create($node_info);
    $node->save();
  }
}