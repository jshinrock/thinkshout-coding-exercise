<?php

namespace Drupal\stripe_donation_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Donation Form' Block.
 *
 * @Block(
 *   id = "stripe_donation_block",
 *   admin_label = @Translation("Stripe donation form"),
 *   category = @Translation("Donations"),
 * )
 */
class DonateFormBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return \Drupal::formBuilder()->getForm('Drupal\stripe_donation_block\Form\DonationForm');
  }

  // public function blockForm($form, FormStateInterface $form_state) {
  // 	CONFIG for block here if needed. Not sure it is for this exercise
  // }

  // public function blockSubmit($form, FormStateInterface $form_state) {
  // 	SUBMIT HANDLER for CONFIG section above. purposfully omitted from exercise
  // }

}