##ThinkShout coding exercise

####Guidelines:

* Use GitHub, Bitbucket or similar to host your work.
* In a text document in the project or in issues in the project outline the specific requirements needed to implement the above request.
* Use a simple Drupal 8 site as a starting point. Your site with the donation form should ideally work with two installation steps: 1) installing Drupal (this can include composer install) 2) enabling one custom module containing your configuration and custom code.
* You are encouraged to include your thoughts about why you took the approach you did, what was easy or hard, etc.


####Extra credit:
* Allow the user to select from a list of donation amounts or enter a custom value.
* Track the status of the transaction from Stripe in Drupal with a record of the donation.
* Use AJAX to validate the form inputs.

##Jeff's thoughts

####POC considerations
* I am purposely NOT investigating additional modules (other than the Stripe library) that might already provide some of the functionality. I _think_ the goal of the exercise is to show that I can competently a) write a module and b) make API calls to an external service. If I was investigating a solution for a customer, I would have to look at contributed modules to determine if some or all of this functionality is present.
* Devel has been installed to assist with the development of this module

####Noted ommissions from POC
* it would be possible to pull user information from the current user to pre-fill some of the cc form. I'm not sure this is in the scope of the project, but I wanted to note that it would be possible.
* ideally, a permission would be created for the scope of this module. I may get to that in the time allotted, but wanted to jot down a quick note incase I do not.
* I'm choosing not to validate credit card number. I imagine Stripe is much better at that than I am. I will let them handle it.